﻿using MetaLanguage.Attributes;
using MetaLanguage.CodeGenerators.File.Copy;

namespace MetaLanguage.Methods
{
    [MetaLanguageClass]
    public static class File
    {
        [MetaLanguageMethod(
            PascalCodeGenerator = typeof(FileCopyPascalCodeGenerator),
            CSharpCodeGenerator = typeof(FileCopyCSharpCodeGenerator),
            PythonCodeGenerator = typeof(FileCopyPythonCodeGenerator),
            VisualBasicCodeGenerator = typeof(FileCopyVisualBasicCodeGenerator))]
        public static bool Copy(string source, string destination)
        {
            return true;
        }
    }
}
