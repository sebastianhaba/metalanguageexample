﻿using Common.CodeGenerators;

namespace MetaLanguage.CodeGenerators.File.Copy
{
    public sealed class FileCopyPascalCodeGenerator : ICodeGenerator
    {
        public string GenerateUsing()
        {
            return "pascal using";
        }

        public string GenerateCode()
        {
            return "File($source);";
        }
    }
}
