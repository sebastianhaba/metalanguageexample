﻿using Common.CodeGenerators;

namespace MetaLanguage.CodeGenerators.File.Copy
{
    public sealed class FileCopyCSharpCodeGenerator : ICodeGenerator
    {
        public string GenerateUsing()
        {
            return "c# using";
        }

        public string GenerateCode()
        {
           return "File(string $source$);";
        }
    }
}