﻿using Common.CodeGenerators;

namespace MetaLanguage.CodeGenerators.File.Copy
{
    public sealed class FileCopyVisualBasicCodeGenerator : ICodeGenerator
    {
        public string GenerateUsing()
        {
            return "visual basic using";
        }

        public string GenerateCode()
        {
            return "visual basic code";
        }
    }
}
