﻿using Common.CodeGenerators;

namespace MetaLanguage.CodeGenerators.File.Copy
{
    public class FileCopyPythonCodeGenerator : ICodeGenerator
    {
        public string GenerateUsing()
        {
            return "python using";
        }

        public string GenerateCode()
        {
            return "python code";
        }
    }
}
