﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MetaLanguage.Extensions
{
    public static class ReflectionExtensions
    {
        public static IEnumerable<Type> GetTypesWithAttribute<T>(this Assembly assembly) where T : Attribute
        {
            return assembly.GetTypes().Where(type => type.GetCustomAttributes(typeof(T), true).Length > 0);
        }
    }
}
