﻿using System;

namespace MetaLanguage.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class MetaLanguageClassAttribute : Attribute
    {
    }
}
