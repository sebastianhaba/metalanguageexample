﻿using System;

namespace MetaLanguage.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class MetaLanguageMethodAttribute : Attribute
    {
        public Type PascalCodeGenerator { get; set; }

        public Type CSharpCodeGenerator { get; set; }

        public Type VisualBasicCodeGenerator { get; set; }

        public Type PythonCodeGenerator { get; set; }
    }
}

