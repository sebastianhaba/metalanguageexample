﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Common.Models;
using Common.Services;
using MetaLanguage.Attributes;
using MetaLanguage.Extensions;

namespace MetaLanguage.Services
{
    public class MetaLanguageService : IMetaLanguageService
    {
        public IEnumerable<MetaLanguageMethod> GetMetaLanguageMethods()
        {
            var result = new List<MetaLanguageMethod>();

            var assembly = Assembly.GetAssembly(typeof(MetaLanguageClassAttribute));
            foreach(var metaClass in assembly.GetTypesWithAttribute<MetaLanguageClassAttribute>())
                result.AddRange(from metodInfo in metaClass.GetMethods(BindingFlags.Static | BindingFlags.Public)
                    where metodInfo.GetCustomAttribute<MetaLanguageMethodAttribute>() != null
                    select new MetaLanguageMethod
                    {
                        ClassName = metaClass.FullName,
                        MethodName = metodInfo.Name
                    });

            return result;
        }
    }
}