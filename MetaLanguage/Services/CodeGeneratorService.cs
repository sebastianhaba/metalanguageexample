﻿using System;
using System.Linq;
using System.Reflection;
using Common.CodeGenerators;
using Common.Models;
using Common.Services;
using MetaLanguage.Attributes;

namespace MetaLanguage.Services
{
    public class CodeGeneratorService : ICodeGeneratorService
    {
        public ICodeGenerator GetGenerator(string fullName, string method, LanguageType language)
        {
            try
            {
                if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(method))
                {
                    return null;
                }

                var type = Type.GetType(fullName);
                if (type == null)
                {
                    return null;
                }

                var publicStaticMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Static);
                if (!publicStaticMethods.Any())
                {
                    return null;
                }

                foreach (var publicStatiMethod in publicStaticMethods)
                {
                    if (publicStatiMethod.Name != method)
                    {
                        continue;
                    }

                    var metaLanguageAttributge = publicStatiMethod.GetCustomAttribute<MetaLanguageMethodAttribute>();
                    if (metaLanguageAttributge == null)
                    {
                        return null;
                    }

                    var generatorType = GetGeneratorForLanguage(language, metaLanguageAttributge);
                    if (generatorType == null)
                    {
                        return null;
                    }

                    var generatorInstance = Activator.CreateInstance(generatorType) as ICodeGenerator;
                    return generatorInstance;
                }

                return null;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        private Type GetGeneratorForLanguage(LanguageType language, MetaLanguageMethodAttribute metaLanguageAttributge)
        {
            switch(language)
            {
                case LanguageType.CSharp:
                    return metaLanguageAttributge.CSharpCodeGenerator;

                case LanguageType.VisualBasic:
                    return metaLanguageAttributge.VisualBasicCodeGenerator;

                case LanguageType.Python:
                    return metaLanguageAttributge.PythonCodeGenerator;

                case LanguageType.Pascal:
                    return metaLanguageAttributge.PascalCodeGenerator;

                default:
                    return null;
            }
        }
    }
};