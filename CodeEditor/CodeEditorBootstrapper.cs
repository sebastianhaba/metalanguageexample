﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ActiproSoftware.Text.Languages.DotNet.Reflection;
using ActiproSoftware.Text.Languages.DotNet.Reflection.Implementation;
using ActiproSoftware.Text.Parsing;
using ActiproSoftware.Text.Parsing.Implementation;
using Caliburn.Micro;
using CodeEditor.Services;
using CodeEditor.ViewModels;
using Common.Services;
using MetaLanguage.Services;

namespace CodeEditor
{
    public sealed class CodeEditorBootstrapper : BootstrapperBase
    {
        public CodeEditorBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            base.Configure();
            SimpleContainer = new SimpleContainer();
            SimpleContainer.Singleton<IWindowManager, WindowManager>();
            SimpleContainer.Singleton<IEventAggregator, EventAggregator>();
            SimpleContainer.Singleton<IScriptService, ScriptService>();
            SimpleContainer.Singleton<ICodeGeneratorService, CodeGeneratorService>();
            SimpleContainer.Singleton<IMetaLanguageService, MetaLanguageService>();
            SimpleContainer.Singleton<IShellViewModel, ShellViewModel>();
            SimpleContainer.Singleton<ICodeEditorConductorViewModel, CodeEditorConductorViewModel>();
            SimpleContainer.PerRequest<IMainViewModel, MainViewModel>();
            SimpleContainer.PerRequest<IBrowserViewModel, BrowserViewModel>();
            SimpleContainer.PerRequest<ICodeEditorViewModel, CodeEditorViewModel>();
            SimpleContainer.PerRequest<IMetaLanguageBrowserViewModel, MetaLanguageBrowserViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return SimpleContainer.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return SimpleContainer.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            SimpleContainer.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            AmbientParseRequestDispatcherProvider.Dispatcher = new ThreadedParseRequestDispatcher();
            string appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                @"Haba.Net\CodeEditor\Assembly Repository");
            AmbientAssemblyRepositoryProvider.Repository = new FileBasedAssemblyRepository(appDataPath);

            DisplayRootViewFor<IShellViewModel>();
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            var dispatcher = AmbientParseRequestDispatcherProvider.Dispatcher;
            if (dispatcher != null)
            {
                AmbientParseRequestDispatcherProvider.Dispatcher = null;
                dispatcher.Dispose();
            }

            var repository = AmbientAssemblyRepositoryProvider.Repository;
            repository?.PruneCache();

            base.OnExit(sender, e);
        }

        private SimpleContainer SimpleContainer { get; set; }
    }
}
