﻿using Common.Models;

namespace CodeEditor.Messages
{
    public class OpenCodeEditorMessage
    {
        public OpenCodeEditorMessage(Script script)
        {
            Script = script;
        }

        public Script Script { get; }
    }
}
