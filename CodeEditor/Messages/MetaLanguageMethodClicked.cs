﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models;

namespace CodeEditor.Messages
{
    public class MetaLanguageMethodClicked
    {
        public MetaLanguageMethodClicked(MetaLanguageMethod method)
        {
            Method = method;
        }

        public MetaLanguageMethod Method { get; }
    }
}
