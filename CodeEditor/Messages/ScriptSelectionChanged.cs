﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models;

namespace CodeEditor.Messages
{
    public class ScriptSelectionChanged
    {
        public ScriptSelectionChanged(Script script)
        {
            Script = script;
        }

        public Script Script { get; }
    }
}
