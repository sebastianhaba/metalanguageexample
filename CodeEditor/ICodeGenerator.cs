﻿namespace MetaLanguage.CodeGenerators
{
    public interface ICodeGenerator
    {
        string GenerateUsing();

        string GenerateCode();
    }
}
