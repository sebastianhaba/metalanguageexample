﻿using System.Linq;
using Caliburn.Micro;
using CodeEditor.Messages;

namespace CodeEditor.ViewModels
{
    public class CodeEditorConductorViewModel : 
        Conductor<ICodeEditorViewModel>.Collection.OneActive, 
        ICodeEditorConductorViewModel,
        IHandle<OpenCodeEditorMessage>
    {
        private ICodeEditorViewModel _selectedEditor;

        public CodeEditorConductorViewModel(IEventAggregator eventAggregator)
        {
            Editors = new BindableCollection<ICodeEditorViewModel>();
            EventAggregator = eventAggregator;
            EventAggregator.Subscribe(this);
        }

        public void Handle(OpenCodeEditorMessage message)
        {
            var editor = Editors.FirstOrDefault(e => e.Script?.Id == message.Script?.Id);
            if(editor == null)
            {
                editor = IoC.Get<ICodeEditorViewModel>();
                editor.Script = message.Script;
                Editors.Add(editor);
            }

            SelectedEditor = editor;
        }

        public IObservableCollection<ICodeEditorViewModel> Editors { get; }

        public ICodeEditorViewModel SelectedEditor
        {
            get => _selectedEditor;
            set
            {
                if(_selectedEditor != null)
                {
                    _selectedEditor.IsSelected = false;
                }

                _selectedEditor = value;

                if (_selectedEditor != null)
                {
                    _selectedEditor.IsSelected = true;
                }

                NotifyOfPropertyChange(() => SelectedEditor);
                EventAggregator.PublishOnUIThread(new ScriptSelectionChanged(SelectedEditor.Script));
            }
        }

        private IEventAggregator EventAggregator { get; }

    }
}
