﻿using System;
using System.Text;
using ActiproSoftware.Text;
using ActiproSoftware.Text.Implementation;
using ActiproSoftware.Text.Languages.CSharp.Implementation;
using ActiproSoftware.Text.Languages.DotNet.Reflection;
using ActiproSoftware.Text.Lexing;
using ActiproSoftware.Windows.Controls.SyntaxEditor;
using ActiproSoftware.Windows.Controls.SyntaxEditor.IntelliPrompt;
using ActiproSoftware.Windows.Controls.SyntaxEditor.IntelliPrompt.Implementation;
using Caliburn.Micro;
using CodeEditor.Messages;
using CodeEditor.Services;
using CodeEditor.Views;
using Common.Models;
using Common.Services;

namespace CodeEditor.ViewModels
{
    public class CodeEditorViewModel : Screen, ICodeEditorViewModel, IHandle<MetaLanguageMethodClicked>, ILexerTarget
    {
        private string _name;
        private Script _script;
        private string _content;

        public CodeEditorViewModel(IEventAggregator eventAggregator, ICodeGeneratorService codeGeneratorService)
        {
            CodeGeneratorService = codeGeneratorService;
            EventAggregator = eventAggregator;
            EventAggregator.Subscribe(this);
        }

        public void Handle(MetaLanguageMethodClicked message)
        {
            if(!IsSelected || Script == null)
            {
                return;
            }

            var codeGenerator = CodeGeneratorService.GetGenerator(message.Method.ClassName, message.Method.MethodName,
                Script.Language);
            var @using = codeGenerator.GenerateUsing();
            var code = codeGenerator.GenerateCode();

            var snippet = new CodeSnippet();
            snippet.CodeText = code;
            snippet.CodeKind = CodeSnippetKind.Any;
            snippet.CodeLanguage = "csharp";
            snippet.SnippetTypes = CodeSnippetTypes.Expansion;
            snippet.Declarations.Add(new CodeSnippetLiteralDeclaration()
            {
                IsEditable = true,
                Id = "source",
                DefaultText = "source"
            });

            CodeSnippetTemplateSession session = new CodeSnippetTemplateSession();
            session.CodeSnippet = snippet;
            session.Open(editor.ActiveView);
        }

        private SyntaxEditor editor;

        protected override void OnViewLoaded(object view)
        {
            var codeEditorView = view as CodeEditorView;
            if(codeEditorView == null)
            {
                return;
            }

            var syntaxEditor = codeEditorView.SyntaxEditor;
            if(syntaxEditor == null)
            {
                return;
            }

           editor = syntaxEditor;
//
//            SyntaxLanguageDefinitionSerializer serializer = new SyntaxLanguageDefinitionSerializer();
//            string path = @"Pascal.langdef";
//            ISyntaxLanguage language = serializer.LoadFromFile(path);
//            syntaxEditor.Document.Language = language;


            
            

            //            new SyntaxLanguage();
            //
                        var language = new CSharpSyntaxLanguage();
                        var project = language.GetService<IProjectAssembly>();
                        project.AssemblyReferences.AddMsCorLib();  // MsCorLib should always be added at a minimum
                        project.AssemblyReferences.Add("System");
            
                        syntaxEditor.Document.Language = language;
            language.RegisterService(new CSharpCodeSnippetProvider());


            base.OnViewLoaded(view);
        }

        private void SyntaxEditorOnDocumentTextChanged(object sender, EditorSnapshotChangedEventArgs e)
        {
            var syntaxEditor = sender as SyntaxEditor;
            /*            var parser = syntaxEditor.Document.Language.GetParser();
                        var lexer = syntaxEditor.Document.Language.GetLexer();
                        lexer.Parse(new TextSnapshotRange(syntaxEditor.Document.CurrentSnapshot, 0), this);*/


        }

        public bool IsSelected
        {
            get;
            set;
        }

        public Script Script
        {
            get => _script;
            set
            {
                if(_script == value)
                    return;

                _script = value;
                Name = _script?.Name;
                Content = _script?.Content;
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        private IEventAggregator EventAggregator
        {
            get;
            set;
        }

        private ICodeGeneratorService CodeGeneratorService { get; set; }

        public void OnPostParse(int endOffset)
        {
        }

        public ILexerContext OnPreParse(ref int startOffset)
        {
            return null;
        }

        public bool OnTokenParsed(IToken newToken, ILexicalScopeStateNode scopeStateNode)
        {

            return true;
        }

        public bool HasInitialContext { get; }
    }
}