﻿using Caliburn.Micro;

namespace CodeEditor.ViewModels
{
    public class MainViewModel : Screen, IMainViewModel
    {
        public MainViewModel(
            IBrowserViewModel browserViewModel,
            ICodeEditorConductorViewModel codeEditorConductorViewModel,
            IMetaLanguageBrowserViewModel metaLanguageBrowserViewModel)
        {
            BrowserViewModel = browserViewModel;
            CodeEditorConductorViewModel = codeEditorConductorViewModel;
            MetaLanguageBrowserViewModel = metaLanguageBrowserViewModel;
        }

        public IBrowserViewModel BrowserViewModel { get; }

        public ICodeEditorConductorViewModel CodeEditorConductorViewModel { get; }

        public IMetaLanguageBrowserViewModel MetaLanguageBrowserViewModel { get; }
    }
}
