﻿using Caliburn.Micro;
using CodeEditor.Messages;
using Common.Models;
using Common.Services;

namespace CodeEditor.ViewModels
{
    public class MetaLanguageBrowserViewModel : Screen, IMetaLanguageBrowserViewModel
    {
        public MetaLanguageBrowserViewModel(
            IMetaLanguageService metaLanguageService,
            IEventAggregator eventAggregator)
        {
            Methods = new BindableCollection<MetaLanguageMethod>();
            MetaLanguageService = metaLanguageService;
            EventAggregator = eventAggregator;
        }

        public void OnMethodClicked(MetaLanguageMethod method)
        {
            EventAggregator.PublishOnUIThread(new MetaLanguageMethodClicked(method));
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            Methods.AddRange(MetaLanguageService.GetMetaLanguageMethods());
        }

        public IObservableCollection<MetaLanguageMethod> Methods { get; }

        private IMetaLanguageService MetaLanguageService { get; }

        private IEventAggregator EventAggregator { get; }
    }
}
 