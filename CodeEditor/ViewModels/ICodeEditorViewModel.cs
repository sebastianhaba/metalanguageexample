﻿using Caliburn.Micro;
using Common.Models;

namespace CodeEditor.ViewModels
{
    public interface ICodeEditorViewModel : IScreen
    {
        Script Script { get; set; }

        bool IsSelected { get; set; }
    }
}
