﻿using Caliburn.Micro;
using CodeEditor.Messages;
using Common.Models;
using Common.Services;

namespace CodeEditor.ViewModels
{
    public class BrowserViewModel : Screen, IBrowserViewModel, IHandle<ScriptSelectionChanged>
    {
        private Script _selectedScript;

        public BrowserViewModel(
            IScriptService scriptService,
            IEventAggregator eventAggregator)
        {
            Scripts = new BindableCollection<Script>();
            ScriptService = scriptService;
            EventAggregator = eventAggregator;
            EventAggregator.Subscribe(this);
        }

        public void Handle(ScriptSelectionChanged message)
        {
            _selectedScript = message.Script;
            NotifyOfPropertyChange(() => SelectedScript);
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            Scripts.AddRange(ScriptService.GetAllScripts());
        }

        public IObservableCollection<Script> Scripts { get; }

        public Script SelectedScript
        {
            get => _selectedScript;
            set
            {
                if(_selectedScript == value)
                {
                    return;
                }

                _selectedScript = value;
                NotifyOfPropertyChange(() => SelectedScript);
                
                EventAggregator.PublishOnUIThread(new OpenCodeEditorMessage(_selectedScript));
            }
        }

        private IScriptService ScriptService { get; }

        private IEventAggregator EventAggregator { get; }

    }
}