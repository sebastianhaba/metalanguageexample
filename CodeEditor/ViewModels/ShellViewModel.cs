﻿using Caliburn.Micro;

namespace CodeEditor.ViewModels
{
    public class ShellViewModel : Conductor<IScreen>.Collection.OneActive, IShellViewModel
    {
        public ShellViewModel(IMainViewModel mainViewModel)
        {
            MainViewModel = mainViewModel;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            ActivateItem(MainViewModel);
        }

        private IMainViewModel MainViewModel
        {
            get;
        }
    }
}
