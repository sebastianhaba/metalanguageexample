﻿using System;
using System.Collections.Generic;
using Common.Models;
using Common.Services;

namespace CodeEditor.Services
{
    public class ScriptService : IScriptService
    {
        public IEnumerable<Script> GetAllScripts()
        {
            return new List<Script>
            {
                new Script
                {
                    Id = Guid.NewGuid(),
                    Language = LanguageType.CSharp,
                    Name = "CSharp Script",
                    Description = "cs"
                },
                new Script
                {
                    Id = Guid.NewGuid(),
                    Language = LanguageType.Python,
                    Name = "Python Script",
                    Description = "py"
                },
                new Script
                {
                    Id = Guid.NewGuid(),
                    Language = LanguageType.Pascal,
                    Name = "Pascal Script",
                    Description = "pas"
                },
                new Script
                {
                    Id = Guid.NewGuid(),
                    Language = LanguageType.VisualBasic,
                    Name = "Visual Basic",
                    Description = "vb"
                }
            };
        }
    }
}
