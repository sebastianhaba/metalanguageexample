﻿namespace Common.CodeGenerators
{
    public interface ICodeGenerator
    {
        string GenerateUsing();

        string GenerateCode();
    }
}
