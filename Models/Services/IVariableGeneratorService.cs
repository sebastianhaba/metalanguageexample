﻿using System;
using Common.Models;

namespace Common.Services
{
    public interface IVariableGeneratorService
    {
       string GetVariableForResult(LanguageType language, Guid file);
    }
}
