﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Models;

namespace Common.Services
{
    public interface IScriptService
    {
        IEnumerable<Script> GetAllScripts();
    }
}
