﻿using Common.CodeGenerators;
using Common.Models;

namespace Common.Services
{
    public interface ICodeGeneratorService
    {
        ICodeGenerator GetGenerator(string fullName, string method, LanguageType language);
    }
}
