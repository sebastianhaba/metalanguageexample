﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Models;

namespace Common.Services
{
    public interface IMetaLanguageService
    {
        IEnumerable<MetaLanguageMethod> GetMetaLanguageMethods();
    }
}
