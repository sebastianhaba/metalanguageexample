﻿using System;

namespace Common.Models
{
    public class Script
    {
        public LanguageType Language { get; set; }

        public string Content { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid Id { get; set; }
    }
}
