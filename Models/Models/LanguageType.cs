﻿namespace Common.Models
{
    public enum LanguageType
    {
        CSharp,
        VisualBasic,
        Pascal,
        Python
    }
}
