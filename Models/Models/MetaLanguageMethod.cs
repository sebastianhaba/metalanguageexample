﻿namespace Common.Models
{
    public class MetaLanguageMethod
    {
        public string ClassName { get; set; }

        public string MethodName { get; set; }
    }
}
